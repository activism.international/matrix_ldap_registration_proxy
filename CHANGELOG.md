## 1.1.0

- Support for `/register/available`

## 1.0.0

- Initial version.
