import 'package:json_annotation/json_annotation.dart';

part 'models.g.dart';

@JsonSerializable()
class MatrixRegistrationRequest {
  static final _usernameParser = RegExp(r'^(([a-z0-9\._]+)|@(.+):(.+))$');
  final String? username;
  final String password;
  @JsonKey(defaultValue: true, name: 'bind_email')
  final bool bindEmail;
  @JsonKey(name: 'initial_device_display_name')
  final String? initialDeviceDisplayName;

  MatrixRegistrationRequest({
    this.username,
    required this.password,
    this.bindEmail = false,
    this.initialDeviceDisplayName,
  });

  factory MatrixRegistrationRequest.fromJson(Map<String, dynamic> json) =>
      _$MatrixRegistrationRequestFromJson(json);

  Map<String, dynamic> toJson() => _$MatrixRegistrationRequestToJson(this);

  String get usernameLocalPart =>
      _usernameParser.firstMatch(username!)!.group(2)!.toString();
}

@JsonSerializable()
class MatrixUserAvailable {
  final bool available = true;

  const MatrixUserAvailable();

  factory MatrixUserAvailable.fromJson(Map<String, dynamic> json) =>
      _$MatrixUserAvailableFromJson(json);

  Map<String, dynamic> toJson() => _$MatrixUserAvailableToJson(this);
}

@JsonSerializable()
class MatrixUserTaken {
  @JsonKey(name: 'errcode')
  final String errorCode = 'M_USER_IN_USE';
  final String error = 'Desired user ID is already taken.';

  const MatrixUserTaken();

  factory MatrixUserTaken.fromJson(Map<String, dynamic> json) =>
      _$MatrixUserTakenFromJson(json);

  Map<String, dynamic> toJson() => _$MatrixUserTakenToJson(this);
}
