// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MatrixRegistrationRequest _$MatrixRegistrationRequestFromJson(
        Map<String, dynamic> json) =>
    MatrixRegistrationRequest(
      username: json['username'] as String?,
      password: json['password'] as String,
      bindEmail: json['bind_email'] as bool? ?? true,
      initialDeviceDisplayName: json['initial_device_display_name'] as String?,
    );

Map<String, dynamic> _$MatrixRegistrationRequestToJson(
        MatrixRegistrationRequest instance) =>
    <String, dynamic>{
      'username': instance.username,
      'password': instance.password,
      'bind_email': instance.bindEmail,
      'initial_device_display_name': instance.initialDeviceDisplayName,
    };

MatrixUserAvailable _$MatrixUserAvailableFromJson(Map<String, dynamic> json) =>
    MatrixUserAvailable();

Map<String, dynamic> _$MatrixUserAvailableToJson(
        MatrixUserAvailable instance) =>
    <String, dynamic>{};

MatrixUserTaken _$MatrixUserTakenFromJson(Map<String, dynamic> json) =>
    MatrixUserTaken();

Map<String, dynamic> _$MatrixUserTakenToJson(MatrixUserTaken instance) =>
    <String, dynamic>{};
