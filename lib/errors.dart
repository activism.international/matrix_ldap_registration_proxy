import 'dart:convert';

abstract class MatrixError extends Error {
  String get code;
  String get error;

  String toResponse() {
    return JsonCodec().encode({
      "errcode": code,
      "error": error,
    });
  }
}

class MatrixUserInUse extends MatrixError {
  @override
  String get code => 'M_USER_IN_USE';

  @override
  String get error => 'Desired user ID is already taken.';
}

class MatrixUserExclusive extends MatrixError {
  @override
  String get code => 'M_EXCLUSIVE';

  @override
  String get error => 'Desired user ID is part of an exclusive namespace.';
}
