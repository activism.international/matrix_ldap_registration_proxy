import 'dart:convert';
import 'dart:io';

import 'package:dartdap/dartdap.dart';
import 'package:dotenv/dotenv.dart';
import 'package:http/http.dart';
import 'package:matrix_ldap_registration_proxy/errors.dart';
import 'package:matrix_ldap_registration_proxy/models.dart';

late int port;
late String baseDn;
late String authnUser;
late String authnPassword;
late String ldapServer;
late String ldifTemplate;
late String matrixServerName;
late String matrixServerUrl;

Future<void> main() async {
  readEnvironment();
  final server = await createServer();
  print('Server started: ${server.address} port ${server.port}');
  await handleRequests(server);
}

void readEnvironment() {
  try {
    load();
    // ignore: empty_catches
  } catch (e) {}
  port = Platform.environment['LISTEN_PORT'] != null
      ? int.parse(Platform.environment['LISTEN_PORT']!)
      : env['LISTEN_PORT'] != null
          ? int.parse(env['LISTEN_PORT']!)
          : 8080;
  ldapServer = Platform.environment['LDAP_SERVER'] ?? env['LDAP_SERVER']!;
  baseDn = Platform.environment['LDAP_BASE_DN'] ?? env['LDAP_BASE_DN']!;
  authnUser = Platform.environment['LDAP_USER'] ?? env['LDAP_USER']!;
  authnPassword =
      Platform.environment['LDAP_PASSWORD'] ?? env['LDAP_PASSWORD']!;
  matrixServerName =
      Platform.environment['MATRIX_SERVER_NAME'] ?? env['MATRIX_SERVER_NAME']!;
  matrixServerUrl =
      Platform.environment['MATRIX_SERVER_URL'] ?? env['MATRIX_SERVER_URL']!;
}

Future<HttpServer> createServer() async {
  final address = InternetAddress.anyIPv4;
  return await HttpServer.bind(address, port);
}

Future<void> handleRequests(HttpServer server) async {
  await for (HttpRequest request in server) {
    try {
      switch (request.method) {
        case 'OPTIONS':
          handleOptions(request);
          break;
        case 'POST':
          handlePost(request);
          break;
        case 'GET':
          handleGet(request);
          break;
        default:
          handleDefault(request);
      }
    } on MatrixError catch (e) {
      request.response
        ..statusCode = HttpStatus.badRequest
        ..write(e.toResponse())
        ..close();
    } catch (e) {
      print(e);
      handleDefault(request);
    }
  }
}

/// POST requests
Future<void> handlePost(HttpRequest request) async {
  switch (request.uri.path) {
    case '/register':
      try {
        Map<String, dynamic> jsonData = Map.from(
            JsonCodec().decode(await utf8.decoder.bind(request).join()));
        if (jsonData.keys.length > 1) {
          final registrationData = MatrixRegistrationRequest.fromJson(jsonData);
          final result = await createLdapUser(registrationData);
          if (result != null) {
            request.response
              ..statusCode = HttpStatus.badRequest
              ..write(result.toResponse())
              ..close();
          } else {
            final baseUri =
                Uri.parse(matrixServerUrl + '/_matrix/client/r0/login');
            final response = await post(baseUri,
                body: JsonCodec().encode({
                  "type": "m.login.password",
                  "user": registrationData.usernameLocalPart,
                  "password": registrationData.password,
                  "initial_device_display_name":
                      registrationData.initialDeviceDisplayName,
                }),
                headers: {'Content-Type': 'application/json'});
            request.response
              ..statusCode = response.statusCode
              ..write(response.body)
              ..close();
          }
          return;
        } else {
          // sending `Unauthorized` as Element expects it...
          request.response
            ..statusCode = HttpStatus.unauthorized
            ..headers.add('Content-Type', 'application/json')
            ..write('''{"flows":[{"stages":["m.login.dummy"]}],"params":{}}''')
            ..close();
        }
      } catch (e) {
        request.response
          ..statusCode = HttpStatus.badRequest
          ..write('Unknown error')
          ..close();
      }

      break;
    default:
      request.response
        ..statusCode = HttpStatus.badRequest
        ..write('400 - Bad request')
        ..close();
      break;
  }
}

/// GET requests
Future<void> handleGet(HttpRequest request) async {
  if(request.uri.path.startsWith('/register/available') && request.uri.queryParameters.containsKey('username')){
    final name  = request.uri.queryParameters['username']!;
    final available = await usernameAvailable(name);
    if(available){
      request.response..statusCode=HttpStatus.ok..write(jsonEncode(MatrixUserAvailable().toJson()))..close();
    } else {
      request.response..statusCode=HttpStatus.badRequest..write(jsonEncode(MatrixUserTaken().toJson()))..close();

    }
  }
  else {
    request.response
      ..statusCode = HttpStatus.badRequest
      ..write('400 - Bad request')
      ..close();
  }
}

Future<MatrixError?> createLdapUser(
    MatrixRegistrationRequest registrationData) async {
  final uri = Uri.parse(ldapServer);
  final isLdaps = uri.scheme == 'ldaps';
  final connection = LdapConnection(
    host: uri.host,
    port: uri.port,
    ssl: isLdaps,
    bindDN: authnUser,
    password: authnPassword,
  );

  await connection.open();
  await connection.bind();

  final result = await connection.search(
      baseDn,
      Filter.equals('uid', registrationData.usernameLocalPart),
      ['dn', 'objectclass']);
  if ((!await result.stream.isEmpty)) {
    return MatrixUserInUse();
  }

  final attrs = {
    'objectclass': ['inetOrgPerson', 'organizationalPerson', 'person', 'top'],
    'cn': registrationData.usernameLocalPart,
    'displayname': registrationData.usernameLocalPart,
    'sn': registrationData.usernameLocalPart,
    'uid': registrationData.usernameLocalPart,
    'userPassword': registrationData.password,
  };
  await connection.add(
      'uid=${registrationData.usernameLocalPart},' + baseDn, attrs);
}

Future<bool> usernameAvailable(String username)async{
  final uri = Uri.parse(ldapServer);
  final isLdaps = uri.scheme == 'ldaps';
  final connection = LdapConnection(
    host: uri.host,
    port: uri.port,
    ssl: isLdaps,
    bindDN: authnUser,
    password: authnPassword,
  );

  await connection.open();
  await connection.bind();

  final result = await connection.search(
      baseDn,
      Filter.equals('uid', username),
      ['dn', 'objectclass']);
  return await result.stream.isEmpty;
}

/// Other HTTP method requests
void handleDefault(HttpRequest request) {
  request.response
    ..statusCode = HttpStatus.methodNotAllowed
    ..write('Unsupported request: ${request.method}.')
    ..close();
}

/// Other HTTP options requests
void handleOptions(HttpRequest request) {
  request.response
    ..statusCode = HttpStatus.noContent
    ..close();
}
