# matrix_ldap_registration_proxy

Want to build a large-scale Matrix server using external registration on LDAP?

You are annoyed of all the support requests telling registration is not working just because you are using LDAP?

This proxy handles Matrix registration requests and forwards the to LDAP. Easy to use with `docker-compose`.

**Please note:** This is nether a whole homeserver implementation, nor we support the full Matrix specification for
registrations. We only provide a very coarse implementation of a basic password registration.

## Configuration

- copy `.env.example` to `.env`
- fill in all required variables
- review the docker-compose file
- `docker-compose up -d`
- configure your proxy to forward `/_matrix/client/r0/register` to `your_ldap_registration_proxy_ip:PORT/register` 